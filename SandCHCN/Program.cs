﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SandCHCN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Day la chuong trinh tinh chu vi va dien tich hinh chu nhat");
            Console.WriteLine("Nhap vao chieu dai: ");
            int chieudai = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao chieu rong: ");
            int chieurong = int.Parse(Console.ReadLine());
            Console.WriteLine("Dien tich cua hinh chu nhat la: " + chieudai * chieurong);
            Console.WriteLine("Chu vi hinh cua hinh chu nhat la: " + (chieudai + chieurong)*2);
        }
    }
}
